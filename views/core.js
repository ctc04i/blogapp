var myBlogApp = angular.module('myBlogApp', ["ngRoute"]);

myBlogApp.config(function ($routeProvider) {
    $routeProvider

    .when("/", {
        templateUrl : "posts.html",
        controller : "postsController"
    })

    .when("/post/:id", {
        templateUrl : "post.html",
        controller : "postController"
    })

    .when("/new", {
        templateUrl : "new.html",
        controller : "newController"
    })

    .when("/edit/:id", {
        templateUrl : "edit.html",
        controller : "editController"
    });

});


myBlogApp.controller("postsController", function ($scope, $http) {
    console.log('postsController called once');
    $http.get("/posts")
        .then(function(response) {
            $scope.content = response.data;
            $scope.statuscode = response.status;
            $scope.statustext = response.statusText;
        }, function(response) {
            $scope.content = "Something went wrong";
            $scope.statuscode = response.status;
            $scope.statustext = response.statusText;
        });
});

myBlogApp.controller("postController", function ($scope, $http, $route) {
    console.log('postController called once');
    $http.get("/post/"+$route.current.params.id)
        .then(function(response) {
            $scope.content = response.data;
            $scope.statuscode = response.status;
            $scope.statustext = response.statusText;
        }, function(response) {
            $scope.content = "Something went wrong";
            $scope.statuscode = response.status;
            $scope.statustext = response.statusText;
        });
});

myBlogApp.controller("editController", function ($scope, $http, $route, $location) {
    console.log('editController called once');
    $scope.myForm = {};

    $http.get("/post/"+$route.current.params.id)
        .then(function(response) {
            $scope.content = response.data;
            $scope.statuscode = response.status;
            $scope.statustext = response.statusText;
        }, function(response) {
            $scope.content = "Something went wrong";
            $scope.statuscode = response.status;
            $scope.statustext = response.statusText;
        });

    $scope.savePost = function (id) {
        $scope.myForm.id = id;
        //console.log($scope.myForm);
        $http.post("/post/"+id, $scope.myForm)
            .then(function(response) {
                //console.log(response);
                $location.path("/");
            });
    };

    $scope.deletePost = function (id) {
        $http.delete("/post/"+id,{"datad":id})
            .then(function(response) {
                //console.log(response);
                $location.path("/");
            });
    };

});

myBlogApp.controller("newController", function ($scope, $http, $route, $location) {
    console.log('newController called once');
    $scope.formData = {};
    $scope.newPost = function () {
        //console.log($scope.formData);
        $http.post("/posts", $scope.formData)
            .then(function(response) {
                //console.log(response);
                //console.log($route);
                //console.log($location);
                $location.path("/");
            });
    };
});

