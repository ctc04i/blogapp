var express = require('express'), 
    app = express(),
    assert = require('assert'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("views"));
app.use(bodyParser.json());                                     // parse application/json

var url = "mongodb://localhost:27017/blogapp_db";
var pdb = mongoose.connect(url,{useMongoClient: true}, function(err,db) {
    assert.equal(null,err);
    console.log("Connected to mongodb server")
});

var blogSchema = new mongoose.Schema({
    title: String,
    body: String,
    created: {type: Date, default: Date.now}
});

var Blog = mongoose.model("Blog", blogSchema);


//VIEW ALL POSTS
app.get('/posts', function (req, res) {
    Blog.find({}, function(err,r) {
        assert.equal(null,err);
        res.json(r);             
    });
});

//VIEW INDIVIDUAL POST
app.get('/post/:id', function (req, res) {
    Blog.find({_id: req.params.id}, function(err,r) {
        assert.equal(null,err);
        res.json(r);
    });
});

//POST NEW POST TO DB
app.post('/posts', function (req,res) {
    console.log("new post:");
    console.log(req.body);
    Blog.create({title:req.body.tit,body:req.body.bod}, function(err, r) {
        assert.equal(null, err);                
        console.log('New post added.');
        res.send("nothing");
    });
});

//POST EDITED POST TO DB
app.post('/post/:id', function (req,res) {
    console.log("edit post:");
    console.log(req.body);
    Blog.findOneAndUpdate({_id: req.body.id},{$set:{title:req.body.tit,body:req.body.bod}}, function(err, r) {
        assert.equal(null, err);                
        console.log('Changes to post saved.');
        res.send("nothing");
    });   
});

//POST EDITED POST TO DB
app.delete('/post/:id', function (req,res) {
    //console.log(req.params);
    Blog.remove({_id: req.params.id}, function(err, r) {
        assert.equal(null, err);                
        console.log('Post deleted.');
        res.send("nothing");
    });   
});

app.get('*', function(req, res) {
    res.sendFile('index.html', {root: __dirname + '/views/'}); // load the single view file (angular will handle the page changes on the front-end)
});


app.listen(3000, function(){
    console.log("Server is running");
});

process.on('SIGTERM', function(){
    pdb.close();
});

