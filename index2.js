var express = require('express'), 
    app = express(),
    MongoClient = require('mongodb').MongoClient,
    ObjectID = require('mongodb').ObjectID,
    assert = require('assert'),
    bodyParser = require('body-parser');


var url = 'mongodb://localhost:27017/blogapp_db';
console.log('server is running');

var pdb

MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    console.log("Connected to mongodb server");
    pdb = db;
    
    app.use(bodyParser.urlencoded({ extended: true }));
    app.listen(3000);

    app.get('/', function (req, res) {
        res.redirect('/posts');
    });


    //VIEW LIST OF POSTS. DONE.
    app.get('/posts', function (req, res) {
        pdb.collection('blogs').find({}).toArray(function(err,r) {
            assert.equal(null,err);
            text = '<h1>This is a h1.</h1><table><tr><th>Title</th></tr>';
            for (i=0; i<r.length; i++) {
                text += '<tr><td><a href="http://localhost:3000/posts/'+(i+1)+'">'+r[i].title+'</a></td></tr>';
            }
            text += '</table>';
            text += '<a href="http://localhost:3000/posts/edit/'+(r.length+1)+'">Add new post</a>';
            console.log(req);
            if (req.query.missage != null) {
                text += '<h1>'+req.query.missage+'</h1>';
            }
            res.send(text);
        });
    });


    //EDIT OR ADD NEW POST WITH BUTTON TO DELETE. DONE.
    app.get('/posts/edit/:id', function (req,res) {
        pdb.collection('blogs').find({}).toArray(function(err,r) {
            assert.equal(null,err);
            if (req.params.id > r.length) {
                text = '<form action="/posts/'+req.params.id+'" method="POST"><input type="text" name="titlename" placeholder="new title here"><input type="text" name="bodyname" placeholder="new body here"><input type="submit" value="Add new post"></form>';                
            } else {
                //console.log(r[req.params.id-1].title);
                //console.log(r[req.params.id-1].body);
                text = '<html><head><script src="https://code.jquery.com/jquery-3.2.1.min.js"></script></head><body><form action="/posts/'+req.params.id+'" method="POST"><input type="text" name="titlename" value="'+r[req.params.id-1].title+'""><input type="text" name="bodyname" value="'+r[req.params.id-1].body+'"><input type="submit" value="Submit changes"></form>';
                text += '<button id="thisone">Delete</button><script>$(document).ready(function() {$("#thisone").click(function() {$.ajax({url:"/posts/'+req.params.id+'",type:"DELETE",success:function(result) {window.location.replace("/posts?missage=donez");}});});});</script></body></html>';
                //text += '<form action="/posts/'+req.params.id+'" method="DELETE"><input type="submit" value="Delete"></form><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>';                
            }
            res.send(text);
        });
    });


    //VIEW INDIVIDUAL POSTS. DONE.
    app.get('/posts/:id', function (req,res) {
        pdb.collection('blogs').find({}).toArray(function(err,r) {
            assert.equal(null,err);
            text = '<h2>This is a h2.</h2><table><tr><th>Title</th><th>Body</th></tr><tr><td>'
                +r[req.params.id-1].title+'</td><td>'+r[req.params.id-1].body+'</td><td><a href="http://localhost:3000/posts/edit/'+(req.params.id)+'">Edit</a></td></tr></table>';
            res.send(text);
        });
    });


    //POST NEW/EDITED POST TO DB. DONE.
    app.post('/posts/:id', function (req,res) {
        pdb.collection('blogs').find({}).toArray(function(err,r) {
            assert.equal(null,err);
            if (req.params.id > r.length) {
                pdb.collection('blogs').insertOne({title:req.body.titlename,body:req.body.bodyname}, function(err, r1) {
                    assert.equal(null, err);                
                    assert.equal(1, r1.insertedCount);
                    console.log('new post added');
                    res.redirect('/posts');
                });
            } else
                //console.log(req.params.id);
                //console.log(req.body.titlename);
                //console.log(req.body.bodyname);
                //console.log(r[req.params.id-1]);
                //console.log(r[req.params.id-1]._id);
                pdb.collection('blogs').updateOne({'_id':ObjectID(r[req.params.id-1]._id)}, {$set:{title:req.body.titlename,body:req.body.bodyname}}, function(err, result){
                    assert.equal(null, err);                
                    console.log('post edited');
                    res.redirect('/posts');
                });
        });
    });



    //DELETE POST
    app.delete('/posts/:id', function (req,res) {
        console.log('hit delete route');
        pdb.collection('blogs').find({}).toArray(function(err,r) {
            assert.equal(null,err);
            pdb.collection('blogs').remove({'_id':ObjectID(r[req.params.id-1]._id)}, {justOne: true}, function(err, result){
                assert.equal(null, err);                
                console.log('post deleted');
                res.send(); 
            });
        });
    });



    app.get('*', function (req, res) {
        res.redirect('/');
    });   
});



process.on('SIGTERM', function(){
    pdb.close();
});



/*    MongoClient.connect(url, function(err, db) {
        assert.equal(null, err);
        console.log("Connected to mongodb server");
        db.collection('blogs').find({}).toArray(function(err,r) {
            assert.equal(null,err);
*/
            //console.log(r);
            //console.log(typeof(r));
            //console.log(r[0].title);
