var express = require('express'), 
    app = express(),
    MongoClient = require('mongodb').MongoClient,
    ObjectID = require('mongodb').ObjectID,
    assert = require('assert'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose');


var url = "mongodb://localhost:27017/blogapp_db";
app.set("view engine","ejs");
app.use(bodyParser.urlencoded({ extended: true }));

var pdb = mongoose.connect(url,{useMongoClient: true}, function(err,db) {
    assert.equal(null,err);
    console.log("Connected to mongodb server")
});

var blogSchema = new mongoose.Schema({
    title: String,
    body: String,
    created: {type: Date, default: Date.now}
});

var Blog = mongoose.model("Blog", blogSchema);


app.get('/', function (req, res) {
    res.redirect('/posts');
});


//VIEW ALL POSTS
app.get('/posts', function (req, res) {
    Blog.find({}, function(err,r) {
        assert.equal(null,err);
        res.render("posts", {posts:r, message:req.query.message});             
    });
});


//VIEW INDIVIDUAL POST. DONE
app.get('/posts/:id', function (req, res) {
    Blog.find({}, function(err,r) {
        assert.equal(null,err);
        res.render("post", {post:r[req.params.id-1], id:req.params.id});             
    });
});


//EDIT OR ADD NEW POST WITH BUTTON TO DELETE
app.get('/posts/edit/:id', function (req, res) {
    Blog.find({}, function(err,r) {
        assert.equal(null,err);
        if (req.params.id > r.length) {
            res.render("new", {id:req.params.id});
        } else {
            res.render("edit", {post:r[req.params.id-1], id:req.params.id});
        }
    });
});


//POST NEW/EDITED POST TO DB. DONE.
app.post('/posts/:id', function (req,res) {
    Blog.find({}, function(err,r) {
        assert.equal(null,err);
        if (req.params.id > r.length) {
            Blog.create({title:req.body.titlename,body:req.body.bodyname}, function(err, r1) {
                assert.equal(null, err);                
                //assert.equal(1, r1.insertedCount);
                console.log('New post added.');
                res.redirect('/posts?message=New+post+added');
            });
        } else {
            Blog.findOneAndUpdate({'_id':ObjectID(r[req.params.id-1]._id)}, {$set:{title:req.body.titlename,body:req.body.bodyname}}, function(err, result){
                assert.equal(null, err);                
                console.log('Post edited.');
                res.redirect('/posts?message=Changes+saved');
            });
        }
    });
});


//DELETE POST
app.delete('/posts/:id', function (req,res) {
    console.log('hit delete route');
    Blog.find({}, function(err,r) {
        assert.equal(null,err);
        Blog.remove({'_id':ObjectID(r[req.params.id-1]._id)}, function(err, result){
            assert.equal(null, err);                
            console.log('Post deleted.');
            res.send(); 
        });
    });
});


//OTHERS
app.get('*', function (req, res) {
    res.redirect('/');
});   


app.listen(3000, function(){
    console.log("Server is running");
});

process.on('SIGTERM', function(){
    pdb.close();
});
